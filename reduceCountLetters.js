/*Write a function that takes a string and returns an object 
representing the character count for each letter. 
Use .reduce to build this object. ex. countLetters('abbcccddddeeeee') // => {a:1, b:2, c:3, d:4,
e:5} */
function countLetters(string) {
    const arr = string.split("");
    console.log(arr);
    return arr.reduce(function (allCharacters, character) {
        if (character in allCharacters) {
            allCharacters[character]++;
        }
        else {
            allCharacters[character] = 1;
        }
        return allCharacters;
    }, {});
}

console.log(countLetters("abbcccddddeeeee"));