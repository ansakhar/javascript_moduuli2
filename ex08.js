function fib (n) {
    if (n < 2) return [1];
    if (n < 3) return [1, 1];
    const arr = fib(n - 1);
    arr.push(arr[n - 2] + arr[n - 3]);
    return arr;
}

console.log(fib(6));