function stringConcat(arr) {
    const str = arr.reduce((acc, n) => acc + n, "");
    return str;
}

console.log(stringConcat([1,2,3])); // "123"