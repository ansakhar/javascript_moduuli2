function generateCredentials(name, lastName) {
    let username = "";
    let password = "";
    username = generateUser(name, lastName);
    password = generatePassword(name, lastName);
    console.log(`username: ${username}, password: ${password}`);
}

function generateUser(name, lastName) {
    return  "B" + (new Date().getFullYear()).toString().substring(2)
    + name.substring(0,2).toLowerCase() + lastName.substring(0,2).toLowerCase();
}

function generatePassword(name, lastName) {
    let password = "";

    password += String.fromCharCode(getRndInteger(65, 90)).toLowerCase()
    + name[0].toLowerCase() + lastName.slice(-1).toUpperCase()
    + String.fromCharCode(getRndInteger(33, 47))
    + (new Date().getFullYear()).toString().substring(2);
    return  password;
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

generateCredentials("John", "Doe");