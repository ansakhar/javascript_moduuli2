/*Find the first non-repeating character from a string*/
const str = "aabbooooofffkkccjdddmTTT";
const array = str.split("");
//console.log(array);
let finded = false;

array.forEach(element => {
    if (finded) return;
    if(str.lastIndexOf(element) === str.indexOf(element)) {
        console.log(element);
        finded = true;
    }
});