/*Include how many of the potential voters were in the ages 18-25,
how many from 26-35, how many from 36-55, and how many of each of
those age ranges actually voted.
 */
const voters = [
    {name:"Bob" , age: 30, voted: true},
    {name:"Jake" , age: 32, voted: true},
    {name:"Kate" , age: 25, voted: false},
    {name:"Sam" , age: 20, voted: false},
    {name:"Phil" , age: 21, voted: true},
    {name:"Ed" , age:55, voted:true},
    {name:"Tami" , age: 54, voted:true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];

function voterResults(arr) {
    return arr.reduce(function (results, vote) {
        if((vote.age >= 18)&&(vote.age <= 25)) {
            results.numYoungPeople ++;
            if(vote.voted) results.numYoungVotes++;
        }
        else if ((vote.age >= 26)&&(vote.age <= 35)) {
            results.numMidsPeople ++;
            if(vote.voted) results.numMidVotesPeople++;
        }
        else if ((vote.age >= 36)&&(vote.age <= 55)) {
            results.numOldsPeople ++;
            if(vote.voted) results.numOldVotesPeople++;
        }
        return results;
    }, {numYoungVotes: 0,
        numYoungPeople: 0,
        numMidVotesPeople: 0,
        numMidsPeople: 0,
        numOldVotesPeople: 0,
        numOldsPeople: 0});
}

console.log(voterResults(voters)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/