function calculator(operator, num1, num2) {
    switch (operator) {
        case "+":
            console.log(num1 + num2);
            break;
        case "-":
            console.log(num1 - num2);
            break;
        case "*":
            console.log(num1 * num2);
            break;
        case "/":
            console.log(num1 / num2);
            break;
        default:
            console.log("Can't do that!");
            break;
    }
}
calculator("+", 2, 3); //5
calculator("/", 8, 2); //4
calculator("", 5, 7); //"Can't do that!"

/*
function calculator(operator, num1, num2) {
    const calculate = {
        "+": () => num1 + num2,
        "-": () => num1 - num2,
        "*": () => num1 * num2,
        "/": () => num1 / num2,
        "default": () => "can't do that",
    };
    return calculate[operator] ?
        calculate[operator]() :
        calculate["default"]();
};

console.log(calculator("*", 2, 3));
*/