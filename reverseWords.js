/*Create a programs that reverses each word in a string.
node .\reverseWords.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes*/
const str = process.argv[2];

function wordsReverser(string){
    const str2 = string.split("").reverse().join(""); //words in reverse order
    return str2.split(" ").reverse().join(" "); //words in original order
}
    
console.log(wordsReverser(str));