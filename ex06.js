const arr1 = [1, 2, 3, 4];
const arr2 =[3, 4, 5, 6];

//1
const arr3 = arr1.concat(arr2.filter((item) => arr1.indexOf(item) < 0));
console.log(arr3);

//2
const arr4 = [...new Set([...arr1 ,...arr2])];
console.log(arr4);

//3
for (let index = 0; index < arr1.length; index++) {
    for (let index2 = 0; index2 < arr2.length; index2++) {
        if(arr1[index] === arr2[index2])
            arr2.splice(index2,1);
    }
}
const arr5 = [...arr1,...arr2];
console.log(arr5);