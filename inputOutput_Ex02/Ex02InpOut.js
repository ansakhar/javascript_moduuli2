import fs from "fs";

const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

//1
const saveForecastsToFile = () => {
    fs.writeFile("forecast_data.json" , JSON.stringify(forecast), "utf8", (err) => {
        if (err) {
            console.log("Could not save forecasts to file!" );
        }
        else {
            console.log("success");
            readForecastsFromFile();
            changeTemperature();
            fs.writeFile("forecast_data.json" , JSON.stringify(forecast), "utf8", (err) => {
                if (err) {
                    console.log("Could not save forecasts to file!" );
                }
                else {
                    console.log("success2");
                }
            });

        }
    });
};
let allForecasts = [];
const readForecastsFromFile = () => {
    try {
        const data = fs.readFileSync ("forecast_data.json" , "utf8");
        allForecasts = JSON.parse(data); // allForecasts is some variable
        console.log(allForecasts);

    } catch (e) {
        console.log("No saved data found." );
    }
};

saveForecastsToFile();

function changeTemperature() {
    forecast.temperature = 30;
    console.log(forecast.temperature);
}