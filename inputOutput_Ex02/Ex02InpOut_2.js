import fs from "fs";

const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

let num = 0;
function saveForecastsToFile() {
    fs.writeFile("forecast_data.json" , JSON.stringify(forecast), "utf8", (err) => {
        if (err) {
            console.log("Could not save forecasts to file!" );
        }
        else {
            num ++;
            console.log("success " + num);
            readForecastsFromFile();
        }
    });
}

function readForecastsFromFile() {
    try {
        const data = fs.readFileSync ("forecast_data.json" , "utf8");
        const allForecasts = JSON.parse(data); // allForecasts is some variable
        console.log(allForecasts);
    } catch (e) {
        console.log("No saved data found." );
    }
}

function changeTemperature(temp) {
    forecast.temperature = temp;
    console.log(forecast.temperature);
    saveForecastsToFile();
}

saveForecastsToFile();
setTimeout(function () {changeTemperature(40);}, 2000);
