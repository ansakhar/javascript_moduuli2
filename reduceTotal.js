function total(arr) {
    const sum = arr.reduce((acc, n) => acc + n, 0);
    return sum;
}
 
console.log(total([1,2,3])); // 6