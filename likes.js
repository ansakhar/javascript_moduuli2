function likes(array) {
    const total = array.length;

    switch(total) {
        case 0:
            console.log("no one likes this");
            break;
        case 1:
            console.log( `${array[0]} likes this`);
            break;
        case 2:
            console.log(`${array[0]} and ${array[1]} like this`);
            break;
        case 3:
            console.log( `${array[0]}, ${array[1]} and ${array[2]} like this`);
            break;
        default:
            console.log(`${array[0]}, ${array[1]} and ${total - 2} others`);
      // code block
    }
}
const arr = ["Alex", "Linda", "Ben", "Ivan"];
likes(arr);
/*
const likes = (nameArr) => {
    if (!Array.isArray(nameArr)) return;
    const getString = {
        0: "no one likes this",
        1: `${nameArr[0]} likes this`,
        2: `${nameArr[0]} and ${nameArr[1]} like this`,
        3: `${nameArr[0]}, ${nameArr[1]} and ${nameArr[2]} like this`,
        default: `must be ${nameArr[0]}, ${nameArr[1]} and ${nameArr.length - 2} others`
    };
    return getString[nameArr.length] ?
    getString[nameArr.length] :
    getString["default"];
};*/