function Student(className, mathGrade, finishGrade, historyGrade) {
    this.className = className;
    this.mathGrade = mathGrade;
    this.finishGrade = finishGrade;
    this.historyGrade = historyGrade;
}

Student.prototype.calculateAverage = function() {
    return (this.mathGrade + this.finishGrade + this.historyGrade)/3;
};

Student.prototype.finishYear = function() {
    const year = parseInt(this.className[0]);
    if(year !== 9)
        this.className = (year + 1) + this.className[1];
    else 
    //return console.log("Graduated");
        this.className = "graduated";
};

const Ben = new Student("5B", 7, 7, 8);
console.log(Ben.calculateAverage());
console.log(Ben);
Ben.finishYear();
console.log(Ben);

class Student2 {
    constructor (className, mathGrade, finishGrade, historyGrade) {
        this.className = className;
        this.mathGrade = mathGrade;
        this.finishGrade = finishGrade;
        this.historyGrade = historyGrade;
    }
    
    calculateAverage () {
        return (this.mathGrade + this.finishGrade + this.historyGrade)/3;
    }
    
    tulosta () {
        console.log(`Average of grades is ${this.calculateAverage()}`);
    }

    finishYear () {
        if(parseInt(this.className.substring(0)) !== 9)
            this.className = (parseInt(this.className.substring(0)) + 1).toString() + this.className.substring(1);
        else 
        //return console.log("Graduated");
            this.className = "graduated";
    }
}
const Ben2 = new Student2("9B", 7, 7, 8);
Ben2.tulosta();
Ben2.finishYear();
console.log(Ben2);
