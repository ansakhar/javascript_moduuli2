/*Write a program that takes in any two numbers from the command line, start and end . The program creates and prints an array filled with numbers from
start to end .
*/
const start = parseInt(process.argv[2]);
const end = parseInt(process.argv[3]);

const array = [];
if (start < end) 
    for (let index = start; index <= end; index++) {
        array.push(index);
    }
else if (start > end)
    for (let index = start; index >= end; index--) {
        array.push(index);
    }
console.log(array);