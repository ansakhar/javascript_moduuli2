function flatten(arr) {
    return arr.reduce(function (newArr, array) {
        return newArr.concat(array);
    }, []); 
}

const arrays = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];

console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];