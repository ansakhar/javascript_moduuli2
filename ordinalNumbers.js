/*const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; const ordinals = ['st', 'nd', 'rd', 'th'];
Create program that outputs competitors placements with following way: ['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor 
was Spencer', '4th competitor was Ann', '5th competitor was John', '6th competitor was Joe']*/
const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];
const newArr = competitors.map(function (d, i) {
    if(!ordinals[i])
        return i+1 + "th competitor was " + d;
    else 
        return i+1 + ordinals[i] + " competitor was " + d;
});
console.log(newArr);