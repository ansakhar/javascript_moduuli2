//Generates a random number between min (inclusive) and max (inclusive)
const arr = [];

function randomInt(min, max) {
    const range = max - min + 1;
    return Math.floor(min + Math.random() * range);
}

function pushElem() {
    while(arr.length < 7){
        const elem = randomInt(1, 40);
        if(arr.indexOf(elem) === -1) {
            arr.push(elem);
        }
    }
}

pushElem();
console.log(arr);

const printNumbersForEverysec = (n)=>{
    for (let i = 0; i < n; i++) {
        setTimeout( () =>{
            console.log(arr[i]);
        }, i * 1000);
    }
};
printNumbersForEverysec(arr.length);

//2
/*
const numbers = [];

while(numbers.length < 7) {
    const lottoNumber = randomInt(1, 40);

    if(!numbers.includes(lottoNumber)) {
        numbers.push(lottoNumber);
    }
}
numbers.sort((a, b) => a - b);
console.log(numbers);
let i = 0;
const timer = setInterval(() => {
    if(i === 6)
        clearInterval(timer);
    console.log(numbers[i]);
    i++;
}, 1000); */