/*Write a function that takes a string and a target, and returns 
true or false if the target is present in the string. 
Use .reduce to acomplish this. ex. isPresent('abcd', 'b') // => 
true ex. isPresent('efghi', 'a') // => false */
function isPresent(string, target) {
    const arr = string.split("");
    console.log(arr);
    return arr.reduce(function (result, character) {
        if (character === target) result = true;
        return result;
    }, false);
}

console.log(isPresent("efaghi", "a"));