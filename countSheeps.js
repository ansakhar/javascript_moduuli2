/*Create a program that takes in a number from the command line,
for example node .\countSheep.js 3 and prints a string "1 sheep...2 sheep...3 
sheep... */
const count = process.argv[2];
console.log(count); 
let str = "";
for (let index = 1; index <= count; index++) {
    str += index + " sheep...";
}
console.log(str);