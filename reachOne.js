/*Create a function that given parameter n finds the number of steps it takes to reach number 1 (one) using the following process
If n is even, divide it by 2
If n is odd, multiply it by 3 and add 1*/

function reachOne(number) {
    let step = 0;
    while (number !== 1) {
        if(number % 2 === 0) {
            number /= 2;
            //console.log(number);
            step ++;
        }
        else {
            number = number*3 + 1;
            //console.log(number);
            step ++;
        }
    }
    console.log(step);
}

reachOne(3); //7
reachOne(5); //5

/*
const countStepsToReachOne = (num, steps = 0) => {
    if (num <= 0) {
        return "works only with numbers larger than 0";
    };
    // console.log("steps: " + steps + " value: " + num);
    if (num === 1) {
        return steps;
    } else if (num % 2 === 0) {
        return countStepsToReachOne((num / 2), steps + 1);
    } else {
        return countStepsToReachOne((num * 3 + 1), steps + 1);
    }
};

console.log(countStepsToReachOne(3));
*/