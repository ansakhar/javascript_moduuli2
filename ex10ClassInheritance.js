class Shape {
    
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }

    calculateArea () {
        return this.width * this.height;
    }

    calculateCircumference () {
        return (this.width + this.height)*2;
    }

    print () { 
        console.log(`Area is ${this.calculateArea()}, circumference is ${this.calculateCircumference()}`);
    }
}

class Rectangle extends Shape {

}

const rectangle1 = new Rectangle(2, 4);
rectangle1.calculateArea();
rectangle1.print();

class Circle extends Shape {
    constructor(diameter) {
        super(diameter, diameter); //circle's diameter
    }
    calculateArea () {
        const radius = this.width/2; //circle's diameter/2
        return radius * radius * Math.PI;
    }

    calculateCircumference () {
        return this.width * Math.PI;
    }
}

const circle1 = new Circle(4);
circle1.calculateArea();
circle1.print();

class Triangle extends Shape {

    calculateArea () {
        return this.height * this.width / 2;
    }

    calculateCircumference () {
        const hypotenuse = Math.sqrt((this.width/2)**2+this.height**2);
        return this.width + hypotenuse * 2;
    }
}

const triangle1 = new Triangle(2, 1);
triangle1.calculateArea();
triangle1.print();
