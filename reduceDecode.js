/*Write a function decode that will take a string of number sets and decode 
it using the following
rules:
When each digit of each set of numbers is added together, the resulting sum is the ascii code for a
single letter.
Convert each set of numbers into a letter and discover the secret message!
Try using map and reduce together to accomplish this task.*/
function decode(string) {
    return string.split(" ").map(encodedCharacter => (encodedCharacter.split("")
        .map(luvu => parseInt(luvu)).reduce((sum, n) => sum + n)))
        .map(code => String.fromCharCode(code)).reduce((result, char) => result + char, "");
}

console.log(decode("584131398786538461382741 444521974525439455955 71415168525426614834414214 353238892594759181769 48955328774167683152 77672648114592331981342373 5136831421236 83269359618185726749 2554892676446686256 959958531366848121621517 4275965243664397923577 616142753591841179359 121266483532393851149467 17949678591875681"));
//"secret-message"

//2:
/*
function decode(string) {
    const arr = string.split(" ");
    //console.log(arr);
    return arr.map(function(character) {
        const arrCharacter =  (character.split("")
            .map(luvu => parseInt(luvu))
            .reduce((sum, n) => sum + n));
        return arrCharacter;
    }).map(code => String.fromCharCode(code))
        .reduce((result, char) => result + char, "");
}
*/