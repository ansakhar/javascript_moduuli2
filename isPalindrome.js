const str = process.argv[2];

function checkPalindrome(string){
    const str2 = string.split("").reverse().join("");
    if (str === str2) console.log(`Yes, '${string}' is a palindrome`);
    else console.log(`No, '${string}' is not a palindrome`);
}

checkPalindrome(str);