import fs from "fs";
const readStream = fs.createReadStream("joulu.txt", "utf-8");
const writeStream = fs.createWriteStream("output2.txt");

//1
readStream.on("data", (txt) => {
    console.log(txt);
});

//2, 3
readStream.on("data", (txt) => {
    const regex = /Joulu/g;
    const regex2 = /Lapsilla/g;
    const txt2 =  txt.replace(regex, "kinkku");
    const txt3 = txt2.replace(regex2, "poroilla");
    const array = txt3.split(" ");
    for (let index = 0; index < array.length; index++) {
        console.log(array[index]);
    }
    console.log(array);
    const outputTxt = array.join(" ");
    writeStream.write(outputTxt, (err) => {
        if (err) console.log(err);
        else console.log("success");
    });
});