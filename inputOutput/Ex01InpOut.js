import fs from "fs";
const readStream = fs.createReadStream("joulu.txt", "utf-8");
const writeStream = fs.createWriteStream("output.txt");

//1
readStream.on("data", (txt) => {
    console.log(txt);
});

//2, 3
readStream.on("data", (txt) => {
    const array = txt.split(" ");
    for (let index = 0; index < array.length; index++) {
        if(array[index] === "joulu") array[index] = "kinkku";
        if(array[index] === "Joulu") array[index] = "Kinkku";
        if(array[index] === "puuroo.\r\nJoulu") array[index] = "puuroo.\r\nKinkku";
        if(array[index] === "lapsilla") array[index] = "poroilla";
        if(array[index] === "Lapsilla") array[index] = "Poroilla";
        console.log(array[index]);
    }
    console.log(array);
    const outputTxt = array.join(" ");
    writeStream.write(outputTxt, (err) => {
        if (err) console.log(err);
        else console.log("success");
    });
});