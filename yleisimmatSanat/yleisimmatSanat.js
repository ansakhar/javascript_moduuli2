//Tee ohjelma joka laskee 10 tai 50 yleisintä sanaa kirjasta
import fs from "fs";

function printSanat(n) { 
    fs.readFile("./SherlockHolms.txt", "utf-8", (err, data) => {
        if (err) console.log(err);
        else {
            let data2 = data.split("\r\n"). join(" ");
            const merkit = [",", ".", "?", "”", "“", "*", ":", "]", "["];
            merkit.forEach(element => {
                data2 = (data2.split(element). join(""));
            });
            const sanat = data2.split(" ").filter(sana => sana !== "");
            const lowCaseArr = sanat.map(sana => sana.toLowerCase());
            //console.log(lowCaseArr);

            const counts = [];
            lowCaseArr.forEach(sana => {
                let finded = false;
                for (let i = 0; i < counts.length; i++) {
                    if(counts[i].nimi === sana) {
                        counts[i].counter++;
                        finded = true;
                    }
                }
                if (!finded) counts.push({nimi: sana, counter: 1});
            });

            counts.sort(function(a,b) {
                return b.counter-a.counter;
            });
            //console.log(counts);

            console.log(`${n} yleisintä sanaa kirjasta:`);
            for (let index = 0; index < n; index++) {
                console.log(counts[index]);
            }
        } 
    });
}

printSanat(10);
printSanat(50);
