/*From the elements of this array, 
1. create a new array with only the numbers that are divisible by three.
2. Create a new array from original array (arr), where each number is 
multiplied by 2
3. Sum all of the values in the array using the array method reduce
console.log the result after each step*/
const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

const divisibleThree = arr.filter(n => n%3 === 0);
console.log(divisibleThree);

const multipliedTwo = arr.map(n => n*2);
console.log(multipliedTwo);

const sum = arr.reduce((acc,n) => acc + n, 0);
console.log(sum);